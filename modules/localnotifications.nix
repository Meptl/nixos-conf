{ config, pkgs, ... }:

{
  # systemd.services = {
  #   localnotifications = {
  #     description = "Listen on a socket. Send notifications if anything hits it.";
  #     requires = [ "graphical.target" ];
  #     path = [ pkgs.coreutils ];
  #     serviceConfig = {
  #       Type = "oneshot";
  #       ExecStart = "${pkgs.python3}/bin/python ${./localnotifications.py}${../configs/pptable_vega56_undervolt} /sys/class/drm/card0/device/pp_table";
  #     };
  #   };
  # };
  networking.firewall = {
    allowedTCPPorts = [ 14693 ];
  };
}
