# Includes general X server programs and configs

{ config, pkgs, ... }:

{
  environment.systemPackages =
  let
    # firefox = pkgs.latest.firefox-nightly-bin or pkgs.firefox;
  in
    with pkgs; [
      entr  # fs watcher used in personal startup scripts

      dunst libnotify
      xclip xdotool xorg.xrandr autorandr

      gnome3.nautilus

      # i3-layout-manager script
      rofi

      # i3 junk
      i3lock-fancy autotiling pywal feh
    ];

  services.xserver = {
    enable = true;
    libinput = {
      enable = true;
      mouse.middleEmulation = false;
    };
    videoDrivers = [ "intel" "nouveau" "amdgpu" "modesetting" ];
    wacom.enable = true;
    xkbOptions = "ctrl:nocaps";
    displayManager.lightdm = {
      enable = true;
      background = ./noise.png;
      greeters.mini = {
        enable = true;
        user = "yutoo";
        extraConfig = ''
          [greeter]
          show-password-label = false
          invalid-password-text = ""
          show-image-on-all-monitors = true
          [greeter-theme]
          background-color = "#060506"
          window-color = "#a285cb"
          border-color = "#a285cb"
          password-color = "#333333"
        '';
      };
    };
    windowManager.i3.enable = true;
  };

  services = {
    redshift = {
      enable = false;
      temperature.night = 3000;
      extraOptions = [
        # UTC.
        "-l 38:-9"
      ];
    };
  };
}
