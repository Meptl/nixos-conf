{ config, lib, pkgs, ... }:

{
  programs.bash.promptInit = ''
    git_entry() {
        git status 2>/dev/null 1>&2
        if [[ "$?" = "0" ]]; then
            branch=$(git status 2>/dev/null | grep "On branch" | awk '{print $3}')
            echo -n " [$branch]"
        fi
    }

    PS1="[\u@\h \W]\$(git_entry) \$ ";
  '';

  programs.bash.interactiveShellInit = ''
    export EDITOR=vim
    export HISTCONTROL=ignoreboth:erasedups
    export HISTSIZE=20000

    unset SSH_ASKPASS

    # Colors
    dircolors &> /dev/null
    if [ $? -eq 0 ]; then
      alias ls='ls --color=auto'
      alias grep='grep --color=auto'
    fi

    alias cd='pushd . &> /dev/null; cd'
    alias pp='popd > /dev/null; pwd'
    alias xclip='xclip -selection c'
    alias vim='nvim'

    # Prevent Ctrl-S from locking terminal
    stty -ixon
  '';

  # This will add a cronjob that cleanups the shell history for all normal
  # users.  This is run every other day at 17:00. I've taken this out since
  # I've moved to xonsh.
  services.cron.systemCronJobs =
  let
    user_by_name = name: builtins.getAttr name config.users.extraUsers;
    # builtins.filter expects a list, which explains this roundabout usage.
    normal_users = builtins.filter (name: (user_by_name name).isNormalUser) (builtins.attrNames config.users.extraUsers);
    clean_command = name: let
      excluded_commands = ["ls" "cd" "pwd" "rm" "ag" "touch"];
      hist_file = name: if (user_by_name name).shell == pkgs.zsh then ".zsh_history" else ".bash_history";
      sed_filter_gen = cmd: ["^${cmd} .*" "^${cmd}$"];  # e.g. sed expr to delete both 'ls' and 'ls some_folder'
      sed_filter = cmd: lib.concatStrings(lib.intersperse ''\|'' (builtins.concatLists (builtins.map sed_filter_gen cmd)));
    in lib.concatStrings (lib.intersperse " && " [  # crontab doesn't support continuation character \
      ''cat .bash_history | nl | sort -k 2 | uniq -f 1 | sort -n | cut -f 2 > ${hist_file name}.bak''
      ''mv ${hist_file name}.bak ${hist_file name}''
      ''sed -ie '/${sed_filter excluded_commands}/d' ${hist_file name}''
    ]);
    # Note that cronjobs are executed relative to $HOME
    crontab = name: "00 17 */2 * * ${name} ${clean_command name}";
    cron_jobs = builtins.map crontab normal_users;
  in
    []; # cron_jobs;
}
