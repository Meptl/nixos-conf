{ config, lib, pkgs, ... }:

{
  programs.hyprland.enable = true;

  environment.systemPackages = with pkgs; [
    libnotify
    pywal

    hyprpaper  # Wallpaper
    wl-clipboard  # clipboard
    rofi-wayland
    wl-color-picker

    grim  # screenshot
    slurp  # screen region selector

    kooha  # screen recorder

    xorg.xrandr  # Still need to set primary monitor for X11 apps.
    xorg.xlsclients  # List XWayland clients. Here while I debug.

    xdg-utils  # xdg-open

    waybar
  ];

  services.greetd = {
    enable = true;
    settings = rec {
      initial_session = {
        command = "${pkgs.hyprland}/bin/Hyprland";
        user = "yutoo";
      };
      default_session = initial_session;
    };
  };
}
