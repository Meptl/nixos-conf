{ config, pkgs, inputs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    inputs.nixos-hardware.nixosModules.common-cpu-intel
    inputs.nixos-hardware.nixosModules.common-pc-laptop
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.nixos-hardware.nixosModules.common-pc
    ../../modules/audio.nix
    ../../modules/desktop-wayland.nix
  ];

  users.extraUsers.yutoo.hashedPasswordFile = config.sops.secrets."passwords/bounce".path;
  sops.secrets."passwords/bounce".neededForUsers = true;

  networking.hostName = "bounce";

  musnix.enable = true;


  programs.light.enable = true; # Backlight

  programs.steam.enable = true;
  programs.gamemode = {
    enable = true;
    settings = {
      custom = {
        start = "${pkgs.libnotify}/bin/notify-send 'GameMode started'";
        end = "${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
      };
    };
  };

  environment.systemPackages = with pkgs; [
    firefox
    blender super-slicer-beta

    signal-desktop
    webcord


    libimobiledevice
    ifuse
    libheif
  ];

  services.usbmuxd.enable = true;
}
