# Host-specific configuration
{ config, lib, pkgs, inputs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    inputs.nixos-hardware.nixosModules.common-cpu-amd
    inputs.nixos-hardware.nixosModules.common-gpu-amd
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.nixos-hardware.nixosModules.common-pc
    ../../modules/audio.nix
    ../../modules/desktop-wayland.nix
    # ../../modules/desktop.nix
    ../../modules/single_gpu_passthrough.nix
    ../../modules/rocm.nix
    ../../modules/localnotifications.nix
  ];

  users.extraUsers.yutoo.hashedPasswordFile = config.sops.secrets."passwords/fume".path;
  sops.secrets."passwords/fume".neededForUsers = true;

  musnix.enable = true;
  services.sshd.enable = false;

  virtualisation.waydroid.enable = true;

  nixpkgs.overlays = [
    (final: prev: {
      # discord = prev.discord.override { nss = final.nss_latest; };
    })
  ];

  nixpkgs.config = {
    allowUnfree = true;
  };

  nix.settings = { "max-jobs" = 12; };

  networking = {
    hostName = "fume";
    enableIPv6 = true;
    firewall.allowedTCPPorts = [
    ];
    firewall.allowedUDPPorts = [
    ];
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [
    "amdgpu.ppfeaturemask=0xfffd7fff"
  ];
  boot.kernelModules = [
    "v4l2loopback"  # Pretty sure this is for OBS screen recording...
  ];

  services.flatpak.enable = true;
  xdg.portal = {
    enable = true;
    # Sway.
    # wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  environment.systemPackages = with pkgs; [
    ardour x42-avldrums calf zynaddsubfx zam-plugins tap-plugins
    yabridge yabridgectl

    sfizz  # Soundfont player (for piano)
    # soundux
    audacity
    ffmpeg

    firefox
    signal-desktop
    webcord
    element-desktop
    # ecryptfs
    # ecryptfs-helper

    openscad
    # musescore

    # super-slicer-beta  # 3D Printer slicing
    # transmission  # torrent client

    aseprite-unfree
    krita
    blender-hip
    godot_4

    obs-studio
  ];

  # udev rules.
  # Installing openocd also applies this rule.
  services.udev.extraRules = ''
    # ST-Link/V2.1
    ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374b", GROUP="users", MODE="0664"
    # Black Magic Probe v2.1
    ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="6018", GROUP="users", MODE="0664"
    # STM Black Pill Keyboard
    ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", GROUP="users", MODE="0664"
    # YD68
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="05df", GROUP="users", MODE="0664"
    # YD68
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="0478", GROUP="users", MODE="0664"
    # TekPower TC4000ZC
    ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", GROUP="users", MODE="0664"
    # Watchy
    ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", GROUP="users", MODE="0664"
  '';
  # services.udev.packages = with pkgs; [ platformio ];

  programs.steam.enable = true;
  programs.gamemode = {
    enable = true;
    settings = {
      custom = {
        start = "${pkgs.libnotify}/bin/notify-send 'GameMode started'";
        end = "${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
      };
    };
  };

  hardware.bluetooth.enable = true;
}
