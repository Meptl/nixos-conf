{ config, pkgs, inputs, ... }@attrs:

{
  users.mutableUsers = false;
  users.extraUsers.yutoo = {
    isNormalUser = true;
    extraGroups = [
      "audio"
      "docker"
      "kvm"
      "libvirtd"
      "lpadmin"
      "networkmanager"
      "podman"
      "render"
      "video"
      "wheel"
    ];
    uid = 1000;
    shell = pkgs.zsh;
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age = {
      keyFile = "/var/lib/sops-nix/key.txt";
    };
    secrets = {
      "openai/api-key" = {
        owner = config.users.users.yutoo.name;
      };
      "huggingface/api-key" = {
        owner = config.users.users.yutoo.name;
      };
    };
  };

  # IPv6 seems to just hang.
  # programs.ssh.extraConfig = "AddressFamily inet";

  system.stateVersion = "22.05";

  fonts = {
    packages = with pkgs; [
      inputs.meptlpkgs.packages.x86_64-linux.sharetechmono
    ];
    fontconfig = {
      defaultFonts = {
        serif = [ "DejaVu Serif" ];
        sansSerif = [ "DejaVu Sans" ];
        monospace = [ "Share Tech Mono" ];
      };
    };
  };

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  nixpkgs.config.allowUnfree = true;

  nix = {
    settings = {
      auto-optimise-store = true;
      substituters = [
        "https://nix-community.cachix.org"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
      # Configured https://digitallyinduced.cachix.org binary cache in /home/yutoo/.config/nix/nix.conf
      trusted-users = [ "root" "yutoo" ];
    };
    extraOptions = ''
      experimental-features = nix-command flakes

      keep-outputs = true
      keep-derivations = true
    '';
  };

  boot = {
    plymouth.enable = true;
    loader.systemd-boot.enable = true;
    loader.systemd-boot.editor = false;
    loader.timeout = 1;
    loader.efi.canTouchEfiVariables = true;
  };

  # Used in home-manager theme config.
  programs.dconf.enable = true;

  networking = {
    networkmanager.enable = true;
    nameservers = [ "208.67.222.222" "208.67.220.220" "1.1.1.1" "1.0.0.1"  ];
    extraHosts = ''
    '';
    # Disable zeroconf, link-local ipv4, 169.254.0.0/16 addresses.
    dhcpcd.extraConfig = "noipv4ll";
  };

  # Enable zsh auto completion for system packages e.g. systemd.
  environment.pathsToLink = [
    "/share/zsh"
  ];

  programs.direnv.enable = true;

  # For use with nix-alien-ld
  programs.nix-ld.enable = true;

  environment.systemPackages = with pkgs; [
    inputs.nix-alien.packages.x86_64-linux.nix-alien

    acpi alsa-utils bashInteractive cacert binutils-unwrapped dnsutils exfat file
    git htop lsof iw patchelf pciutils unzip usbutils wget zip

    at home-manager fd fzf ripgrep tldr tree
    direnv python312 nix-index
    ipad_charge  # iPhone charging via USB.
    # sshuttle # Poor man's VPN. Ssh tunnel.

    inputs.tabbed.packages.x86_64-linux.tabbed
    jq keepassxc mpv
    gron # Better jq

    sops

    # We don't use services.syncthing because the system config stores things in
    # /var/lib and the home-configuration version doesn't create a service to
    # spawn. We'll just run from the commandline whenever we need.
    syncthing
  ];

  programs.zsh.enable = true;
  services.cron.enable = true;
  services.atd.enable = true;
  services.openvpn.servers = {
    vpn0 = {
      config = ''config /etc/nixos/vpn0.conf'';
      autoStart = false;
      updateResolvConf = true;
    };
  };

  # virtualisation.podman = {
  #   enable = true;
  #   dockerCompat = true;
  #   dockerSocket.enable = true;
  # };
  virtualisation.docker.enable = true;

  services.udev.extraRules = ''
    # Enable BFQ IO Scheduling algorithm
    ACTION=="add|change", KERNEL=="[sv]d[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
    ACTION=="add|change", KERNEL=="[sv]d[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"
    ACTION=="add|change", KERNEL=="nvme[0-9]n1", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"
  '';

  services.earlyoom = {
    enable = true;
    enableNotifications = true;
  };

  i18n.inputMethod = {
    enabled = "ibus";
    ibus.engines = with pkgs.ibus-engines; [
      libpinyin
      uniemoji
    ];
  };
}
